// 3) Explicar:
// * .map y pintado automático
// * componentes privados

import Work from './Work';
import './Portfolio.scss';

function Portfolio({ portfolio }) {
    return (
      <section className="Portfolio">
        <h2 className="Portfolio__title">Portfolio</h2>
        <div className="Portfolio__wrap">
          {portfolio.map((work, index) => <Work key={work.alt + index} work={work}/>)}
        </div>
      </section>
    );
}

export default Portfolio;