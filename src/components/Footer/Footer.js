import './Footer.scss';

function Footer({ footer }) {
  return (
    <footer className="Footer">
      <a href='https://www.freepik.es/vectores/tecnologia' className="Footer__link">www.freepik.es</a>
      <span className="Footer__info">{footer.info}</span>
      <span>
        <a href={footer.linkedIn} className="Footer__link">LinkedIn</a>
        <a href={`mailto:${footer.email}`} className="Footer__link">E-mail</a>
      </span>
    </footer>
  );
}

export default Footer;