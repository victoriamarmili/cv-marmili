// 4) Explicar:
// * Hook useState
// * toggle
// * virtual dom

import { useState } from 'react';
import Descriptions from './Descriptions';
import './Curriculum.scss';

function Curriculum({ curriculumSections }) {
    const [showExperience, setExperience] = useState(false)
    const { experience, education } = curriculumSections;

    return (
        <section className="Curriculum">
            <nav>
                <button className={`Curriculum__selection Curriculum__selection--${!showExperience}`} onClick={() => setExperience(false)} >Education</button>
                <button className={`Curriculum__selection Curriculum__selection--${showExperience}`} onClick={() => setExperience(true)} >Experience</button>
            </nav>
            {showExperience
                ? <Descriptions descriptions={experience}/>
                : <Descriptions descriptions={education}/>
            }
        </section>
    );
}

export default Curriculum;