// 2) Explicar:
// * primer componente simple que recibe info desde App
// * index si, index no :)
// * componentes funcionales

import './Hero.scss';
import Developer from '../../assets/developer.svg';

function Hero({ hero }) {
  return (
    <section className="Hero">
      <div className="Hero__info">
        <h1>{hero.name}</h1>
        <h2 className="Hero__title">{hero.profession}</h2>
        <ul>
          <li>
            <span className="Hero__icon">📲</span>
            <a href={`tel:${hero.email}`} className="Hero__link">{hero.mobile}</a>
          </li>
          <li>
            <span className="Hero__icon">📩</span>
            <a href={`mailto:${hero.email}`} className="Hero__link">{hero.email}</a>
          </li>
        </ul>
      </div>
      <div className="Hero__image">
        <img src={Developer} alt="Women Developer"/>
      </div>
    </section>
  );
}

export default Hero;