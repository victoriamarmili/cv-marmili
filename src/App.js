// 1) Explicar:
// * cómo componentizar y pasar props
// * componentes publicos usar Hero
// * boiler plate

import './App.scss';
import Hero from './components/Hero';
import Curriculum from './components/Curriculum';
import Portfolio from './components/Portfolio';
import Footer from './components/Footer';
import { FAKE_DB } from './fakeDB/fakeDB';

const { hero, curriculumSections, portfolio, footer } = FAKE_DB;

function App() {
  return (
    <div className="App">
      <Hero hero={hero}></Hero>
      <Curriculum curriculumSections={curriculumSections}></Curriculum>
      <Portfolio portfolio={portfolio}></Portfolio>
      <Footer footer={footer}></Footer>
    </div>
  );
}

export default App;
