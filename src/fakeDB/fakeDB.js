import Img1 from '../assets/portfolio1.png';
import Img2 from '../assets/portfolio2.png';
import Img3 from '../assets/portfolio3.png';
import Img4 from '../assets/portfolio4.png';
import Img5 from '../assets/portfolio5.png';
import Img6 from '../assets/portfolio6.png';

export const FAKE_DB = {
    hero: {
        name: "María Gonzalez",
        profession: "Frontend Developer",
        mobile: "+34 621 000-000",
        email: "mariagonzalez@gmail.com",
    },
    curriculumSections: {
        education: [
            {
                title: "Fronted developer",
                year: 2021,
                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod "
                    + "tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis "
                    + "nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
            },
            {
                title: "Degree in management",
                year: 2019,
                description: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore"
                    + " eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in "
                    + " culpa qui officia deserunt mollit anim id est laborum."
            },
            {
                title: "English",
                year: 2015,
                description: "Vel quam elementum pulvinar etiam non. Sem et tortor consequat id. Tempor "
                    + "id eu nisl nunc mi ipsum. Vulputate enim nulla aliquet porttitor lacus luctus accumsan"
                    + " tortor posuere."
            },
        ],
        experience: [
            {
                title: "Freelance",
                year: 2021,
                description: "Sollicitudin nibh sit amet commodo nulla facilisi nullam. Commodo viverra "
                    + "maecenas accumsan lacus vel facilisis. Cras sed felis eget velit. Cursus mattis "
                    + "molestie a iaculis at. Morbi tempus iaculis urna id. Eu mi bibendum neque egestas "
                    + "congue quisque egestas diam in. Porttitor eget dolor morbi non. Augue lacus viverra vitae"
                    + " congue eu. Amet porttitor eget dolor morbi non arcu risus quis."
            },
            {
                title: "Customer service",
                year: 2020,
                description: "Nibh sit amet commodo nulla facilisi. Elit duis tristique sollicitudin nibh sit"
                    + " amet commodo. Convallis tellus id interdum velit laoreet id. Turpis in eu mi bibendum "
                    + "neque egestas congue quisque. Morbi blandit cursus risus at ultrices mi tempus imperdiet. "
                    + "Morbi blandit cursus risus at ultrices mi. Condimentum id venenatis a condimentum. Elit "
                    + "ut aliquam purus sit amet luctus venenatis lectus magna."
            }
        ]
    },
    portfolio: [
        {
            url: Img1,
            alt: "Naturitas"
        },
        {
            url: Img2,
            alt: "Scss"
        },
        {
            url: Img3,
            alt: "React"
        },
        {
            url: Img4,
            alt: "graphics"
        },
        {
            url: Img5,
            alt: "NaturLab"
        },
        {
            url: Img6,
            alt: "Game"
        },
    ],
    footer: {
        info: "Code with love 💕",
        linkedIn: "https://www.linkedin.com/feed/",
        email: "mariagonzalez@gmail.com"
    },
}